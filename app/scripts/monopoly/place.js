var MONOPOLY = MONOPOLY || {};

/**
 * Monopoly Place Constructor
 * @param  {Object} options
 */
MONOPOLY.Place = function (options) {};

/**
 * Monopoly Place Prototype
 * @return {Object}
 */
MONOPOLY.Place.prototype = function () {
    /**
     * Get the place type
     * @return {[type]}
     */
    var getPlaceType = function () {
            return this.placeType;
        },
        /**
         * Get the place owner
         * @return {String}
         */
        getPlaceOwner = function () {
            return this.owner.name;
        },
        getPlacePrice = function () {
            return this.price;
        };
        // Private Methods / Properties
        // ...

    return {
        getPlaceType: getPlaceType,
        getPlaceOwner: getPlaceOwner,
        getPlacePrice: getPlacePrice
    };
}();

/**
 * Monopoly Place Factory
 * @return {Object} The new Place created
 */
MONOPOLY.Place.PlaceFactory = function (placeType, options) {
    var newPlace = {};

    // Check if the constructor exists
    if (typeof MONOPOLY.Place[placeType] !== 'function') {
        throw {
            name: 'Error',
            message: placeType + ' constructor doesn\'t exists'
        }
    }

    // Inherit the parent
    if (typeof MONOPOLY.Place[placeType].getPlaceType !== 'function') {
        MONOPOLY.Place[placeType].prototype = new MONOPOLY.Place(options);
    }

    // Create the new place
    newPlace = new MONOPOLY.Place[placeType](options);

    return newPlace;
}

// Constructor for MAYOR OFFICE PLACE
MONOPOLY.Place.MayorOffice = function (options) {
    this.name      = options.name || 'The Mayor\'s office';
    this.basicRent = options.basicRent || 0;
    this.placeType = options.placeType || 'Mayor Office';
};

// Constructor for POLICA STATION
MONOPOLY.Place.PoliceStation = function (options) {
    this.name      = options.name || 'Town\'s Police Station';
    this.basicRent = options.basicRent || 0;
    this.placeType = options.placeType || 'Police Station';
};

// Constructor for JAIL PLACE
MONOPOLY.Place.Jail = function (options) {
    this.name      = options.name || 'Town\'s Jail';
    this.basicRent = options.basicRent || 0;
    this.placeType = options.placeType || 'Jail';
};

// Constructor for JAIL PLACE
MONOPOLY.Place.Default = function (options) {
    // Place name
    this.name            = options.name || 'Place default name';
    // Place rent prices
    this.basicRent       = options.basicRent || 1000;
    this.oneHouseRent    = options.oneHouseRent || this.basicRent * 1.5;
    this.twoHousesRent   = options.twoHousesRent || this.basicRent * 2.5;
    this.threeHousesRent = options.threeHousesRent || this.basicRent * 3.5;
    this.hotelRent       = options.hotel || this.basicRent * 5;
    // house / hotel buy prices
    this.singlePrice     = options.singlePrice || this.basicRent * 1.5;
    this.housePrice      = options.housePrice || this.basicRent * 2;
    this.hotelPrice      = options.hotel || this.basicRent * 3;
    // Place type, e.g. Mayor Office / Police Station / Jail / Default
    this.placeType       = options.placeType || 'default';
    // Place owner
    this.owner = new MONOPOLY.Player({type: 'bank', name: 'The Bank'});
};