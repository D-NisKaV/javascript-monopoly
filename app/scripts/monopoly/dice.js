var MONOPOLY = MONOPOLY || {};

/**
 * Monopoly Dice Constructor
 */
MONOPOLY.Dice = function () {};

/**
 * Monopoly Dice Prototype
 * @return {[type]} [description]
 */
MONOPOLY.Dice.prototype.roll = function () {
    return Math.floor( Math.random(1,7) * (7-1) ) + 1;
};